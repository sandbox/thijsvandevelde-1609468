<?php
function basetheme_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['theme_settings']['ie_mediaqueries'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add mediaqueries for Internet Explorer'),
    '#default_value' => theme_get_setting('ie_mediaqueries')
  );
  $form['theme_settings']['force_google_frame'] = array(
    '#type' => 'checkbox',
    '#title' => t('Force Google Frame on older IE browsers'),
    '#default_value' => theme_get_setting('force_google_frame')
  );

  /**
   * jQuery settings
   * By Thijs
   */
  $form['jquery'] = array(
   '#type' => 'fieldset',
   '#title' => t('Javascript'),
   '#collapsible' => TRUE,
   '#collapsed' => TRUE
  );
  $form['jquery']['jquery'] = array(
    '#type' => 'checkbox',
    '#title' => t('Overload jQuery with newer version'),
    '#default_value' => theme_get_setting('jquery')
  );
  $form['jquery']['jquery_version'] = array(
   '#type'  => 'textfield',
   '#title' => t('jQuery version'),
   '#description' => t('Example: 1.7.0'),
   '#default_value' => theme_get_setting('jquery_version'),
   '#size' => 8,
   '#maxlength' => 10,
  );
  $form['jquery']['jqueryui'] = array(
    '#type' => 'checkbox',
    '#title' => t('Overload jQuery UI with newer version'),
    '#default_value' => theme_get_setting('jqueryui')
  );
  $form['jquery']['jqueryui_version'] = array(
   '#type'  => 'textfield',
   '#title' => t('jQuery UI version'),
   '#description' => t('Example: 1.8.16'),
   '#default_value' => theme_get_setting('jqueryui_version'),
   '#size' => 8,
   '#maxlength' => 10,
  );
  $form['jquery']['load_ajax'] = array(
    '#type' => 'checkbox',
    '#title' => t('Load scripts with Modernizr.load()'),
    '#default_value' => theme_get_setting('load_ajax')
  );
  $form['jquery']['load_scripts'] = array(
    '#type' => 'checkbox',
    '#title' => t('Load scripts.js'),
    '#default_value' => theme_get_setting('load_scripts')
  );
  /**
   * Breadcrumb settings
   * Copied from Zen
   */
  $form['breadcrumb'] = array(
   '#type' => 'fieldset',
   '#title' => t('Breadcrumb'),
   '#collapsible' => TRUE,
   '#collapsed' => TRUE
  );
  $form['breadcrumb']['breadcrumb_display'] = array(
   '#type' => 'select',
   '#title' => t('Display breadcrumb'),
   '#default_value' => theme_get_setting('breadcrumb_display'),
   '#options' => array(
     'yes' => t('Yes'),
     'no' => t('No'),
   ),
  );
  $form['breadcrumb']['breadcrumb_separator'] = array(
   '#type'  => 'textfield',
   '#title' => t('Breadcrumb separator'),
   '#description' => t('Text only. Dont forget to include spaces.'),
   '#default_value' => theme_get_setting('breadcrumb_separator'),
   '#size' => 8,
   '#maxlength' => 10,
  );
  $form['breadcrumb']['breadcrumb_home'] = array(
   '#type' => 'checkbox',
   '#title' => t('Show the homepage link in breadcrumbs'),
   '#default_value' => theme_get_setting('breadcrumb_home'),
  );
  $form['breadcrumb']['breadcrumb_trailing'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append a separator to the end of the breadcrumb'),
    '#default_value' => theme_get_setting('breadcrumb_trailing'),
    '#description'   => t('Useful when the breadcrumb is placed just before the title.'),
  );
  $form['breadcrumb']['breadcrumb_title'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append the content title to the end of the breadcrumb'),
    '#default_value' => theme_get_setting('breadcrumb_title'),
    '#description'   => t('Useful when the breadcrumb is not placed just before the title.'),
  );
  $form['breadcrumb']['breadcrumb_title_link'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Make the title clickable'),
    '#default_value' => theme_get_setting('breadcrumb_title_link'),
    '#description'   => t('Turn the title into a link to the current page.'),
  );
  /**
   * Color settings
   * By Thijs
   */
  $form['misc']['colorize'] = array(
    '#type' => 'checkbox',
    '#title' => t('Color your theme through settings'),
    '#default_value' => theme_get_setting('colorize')
  );

  $form['theme_settings']['#collapsible'] = TRUE;
  $form['theme_settings']['#collapsed'] = TRUE;
  $form['logo']['#collapsible'] = TRUE;
  $form['logo']['#collapsed'] = TRUE;
  $form['favicon']['#collapsible'] = TRUE;
  $form['favicon']['#collapsed'] = TRUE;

  //Collapse standaard Mothership velden
  $form['development']['#collapsed'] = TRUE;
  $form['html5']['#collapsed'] = TRUE;
  $form['css']['#collapsed'] = TRUE;
  $form['css']['nuke']['#collapsed'] = TRUE;
  $form['css']['add']['#collapsed'] = TRUE;
  $form['classes']['#collapsed'] = TRUE;
  $form['classes']['body']['#collapsed'] = TRUE;
  $form['classes']['region']['#collapsed'] = TRUE;
  $form['classes']['block']['#collapsed'] = TRUE;
  $form['classes']['node']['#collapsed'] = TRUE;
  $form['classes']['view']['#collapsed'] = TRUE;
  $form['classes']['field']['#collapsed'] = TRUE;
  $form['classes']['form']['#collapsed'] = TRUE;
  $form['classes']['menu']['#collapsed'] = TRUE;
  $form['Libraries']['#collapsed'] = TRUE;
  $form['misc']['#collapsed'] = TRUE;
}
