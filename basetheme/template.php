<?php

//Function to detect ajax, handig voor ajax pageload...
if (!function_exists('is_ajax')) {

    function is_ajax() {
		return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

}

function basetheme_preprocess_html(&$vars) {
        
	$vars['theme_folder'] = base_path() . path_to_theme();
	
	// Function to cleanup html source a bit more
	function indent($string) {
		$spacing = "        ";
		$string = ltrim(str_replace('<link', $spacing . '<link',  $string));
		$string = ltrim(str_replace('<meta', $spacing . '<meta',  $string));
		$string = ltrim(str_replace('<script', $spacing . '<script',  $string));
		return $string;
	}

}

function basetheme_preprocess_page(&$variables, $hook) {
	
	if(isset($node)){
		$variables['node'] = $node;
	}
	
	$variables['theme_folder'] = base_path() . path_to_theme();
	
  // Set h1 title
  if (isset($variables['node']->the_aim_seo_pagetitle[LANGUAGE_NONE][0]))
    $variables['h1_title'] = $variables['node']->the_aim_seo_pagetitle[LANGUAGE_NONE][0]['safe_value'];
  else
    $variables['h1_title'] = drupal_get_title();
}

function basetheme_process_html(&$vars) {
	//if(!isSet($js)) $js = array();
	//$scripts = drupal_get_js('header', $js);
	
	// Hook into color.module.
	if (theme_get_setting('colorize')) {
		if (module_exists('color')) {
		    _color_html_alter($vars);
		}
	}
	
	$overload = 0;

	//Overload jQuery
	if (theme_get_setting('jquery')) {
		unset($vars['scripts']);
		$js = drupal_add_js('https://ajax.googleapis.com/ajax/libs/jquery/' . theme_get_setting('jquery_version') . '/jquery.min.js', array('type' => 'external', 'scope' => 'header', 'weight' => 0, 'group' => '-200'));
		
		//Overload jQuery UI
		if (theme_get_setting('jqueryui')) {
			$js = drupal_add_js('https://ajax.googleapis.com/ajax/libs/jqueryui/' . theme_get_setting('jqueryui_version') . '/jquery-ui.min.js', array('type' => 'external', 'scope' => 'header', 'weight' => -100, 'group' => '-100'));
			unset($js['misc/ui/jquery.ui.core.min.js']);
		}
		//print_r($js);
		unset($js['misc/jquery.js']);
		//$js['modules/toolbar/toolbar.js']['group'] = -100;
		$vars['scripts'] = drupal_get_js('header', $js);
		
		$scripts = $vars['scripts'];
		$overload = 1;
	}
	/*
	if(theme_get_setting('load_scripts')) {
		$js = drupal_add_js(base_path() . path_to_theme() . '/js/scripts.js', array('type' => 'file', 'scope' => 'header', 'weight' => 1, 'group' => '-1'));
		$scripts = drupal_get_js('header', $js);
	}
	*/
	
	//Scripts inladen via Modernizr.load()
	if (theme_get_setting('load_ajax')) {
		$scripts = "<script>\nModernizr.load([\n\t{\n\tload: [\n" . $scripts;
		$scripts = str_replace('<script type="text/javascript" src="', "\t\t'", $scripts);
		$scripts = str_replace('"></script>', '\',', $scripts);
		$scripts = explode('<script type="text/javascript">', $scripts);
		$scripts = rtrim($scripts[0], ",
 ") . "\n\t],\n\tcomplete: function () {\n\t" . str_replace('</script>', "}\n}]\n);\n</script>", $scripts[1]);
 		$overload = 1;
	}
	
		if($overload) $vars['scripts'] = $scripts;
}

function basetheme_process_page(&$vars, $hook) {
	// Hook into color.module.
	if (theme_get_setting('colorize')) {
		if (module_exists('color')) {
		_color_page_alter($vars);
		}
	}
}

function basetheme_html_head_alter(&$head_elements) {
  unset($head_elements['system_meta_generator']);
  unset($head_elements['system_meta_content_type']);
}


function basetheme_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('breadcrumb_display');
  if ($show_breadcrumb == 'yes') {

    // Optionally get rid of the homepage link.
    $show_breadcrumb_home = theme_get_setting('breadcrumb_home');
    if (!$show_breadcrumb_home) {
      array_shift($breadcrumb);
    }

    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $separator = filter_xss(theme_get_setting('breadcrumb_separator'));
      $trailing_separator = $title = '';

      // Add the title and trailing separator
      if (theme_get_setting('breadcrumb_title')) {
        if ($title = drupal_get_title()) {
        
          if (theme_get_setting('breadcrumb_title_link')) {
            $title = '<a href="' . base_path() . current_path() . '">' . drupal_get_title() . '</a>';
          }
          $trailing_separator = $separator;
        }
      }
      // Just add the trailing separator
      elseif (theme_get_setting('breadcrumb_trailing')) {
        $trailing_separator = $separator;
      }

      // Assemble the breadcrumb
      return t('You are here: ') . implode($separator, $breadcrumb) . $trailing_separator . $title;
    }
  }
  // Otherwise, return an empty string.
  return '';
}