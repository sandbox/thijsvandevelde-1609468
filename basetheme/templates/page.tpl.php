<?php
//dsm(get_defined_vars());
//dsm($theme_hook_suggestions);
// template naming
//page--[CONTENT TYPE].tpl.php
?>

<?php print $mothership_poorthemers_helper; ?>

<?php
/*
  Te gebruiken codes:

  Print een region als er iets in zit:
  <?php if ($page['content'] && !$skip) { print $page['content']; } ?>

  Content region, altijd nodig!
  $page['content']

  Andere nuttige regions:
  $page['help'] // OPGELET! Lijkt niet te werken
  $page['highlighted'] // OPGELET! Lijkt niet te werken
  $page['tabs'] (=editknoppen)
  $page['base_path']
  $page['closure'] (=drupaleigen dingen, zoals admin menu)

  Regions printen?
  print render($page['myregion']);

  Drupal elementen:
  $title 			=> if($title) print $title
  $tabs				=> if($tabs) print render($tabs)
  $breadcrumb		=> if($breadcrumb) print $breadcrumb
  $messages			=> if($messages) print $messages
  $action_links		=> if($messages) print render($action_links)
  $front_page		=> link naar de front (inclusief taalvariabele)

  Enkel frontpage?
  if($is_front) { ... }

  Heeft iemand admin rechten?
  if($is_admin) { ... }			=> if( user_access('administer') ) { ... }

  Anonymous?
  if($logged_in) { ... }		=> if (user_is_logged_in()) { ... }


  Snippets:
  ------------
  <p id="site-name">
  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
  <?php print $site_name; ?>
  </a>
  </p>
 */
?>

<?php if (isset($node)) { ?>
    <h1><?php print $node->metatags['title']['value'];  ?></h1>
<?php } ?>

<div class="container">

    <div id="top">
        <?php print render($page['top']); ?>
    </div>
    
    <header>
        <figure>
            <a href=""><img src="<?php print $theme_folder ?>/img/logo.jpg"></a>
            <figcaption>Onze naam</figcaption>
        </figure>
        <?php print render($page['header']); ?>
        
        <nav>
            <div class='breadcrumb'>
                    <?php print t('You are here: ') . $breadcrumb; ?>
            </div>
            <?php print render($page['main_menu']); ?>
        </nav>
    </header>

    <?php // Sections houden zaken bijeen die bijeen horen. Geven daardoor net iets meer betekenis mee dan een simpele DIV. ?>    
    <section>	
        <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>

        <article>
            <header>                
            </header>
            
            <section class="content">            
                <?php if (user_is_logged_in() && $tabs): ?>
                    <nav class="tabs"><?php print render($tabs); ?></nav>
                <?php endif; ?>

                <?php if ($messages) { ?>
                    <div class="drupal-messages">               
                        <?php print $messages; ?>            
                    </div>
                <?php }//Endif ?>

                <?php print render($page['content']); ?>
                
            </section>
            
            <footer>                
            </footer>
        </article>         
    </section>

    <footer>	
        <?php print render($page['footer']); ?>
    </footer>


</div>

