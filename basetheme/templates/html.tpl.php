<?php
//If ajax or iframe (i.e. Facebook load), only show content
if (is_ajax() || isSet($_GET["content"])) {
    print($page);
} else {
    ?>
<!DOCTYPE html>

<!--[if IEMobile 7 ]><html class="no-js iem7"><![endif]-->
<!--[if lt IE 7 ]><html class="no-js ie6" lang="<?php print $language->language; ?>"><![endif]-->
<!--[if (IE 7)&!(IEMobile) ]><html class="no-js ie7" lang="<?php print $language->language; ?>"><![endif]-->
<!--[if (IE 8)&!(IEMobile) ]><html class="no-js ie8" lang="<?php print $language->language; ?>"><![endif]-->
<!--[if (IE 9)&!(IEMobile) ]><html class="no-js ie9" lang="<?php print $language->language; ?>"><![endif]-->
<!--[if (gt IE 9)|(gt IEMobile 7)|!(IE)]><!--><html class="no-js" lang="<?php print $language->language; ?>"><!--<![endif]--> 

    <?php /* Indeed, Drupal can have a nice source code formatting! :) */ ?>

    <head>
        <title><?php print $head_title; ?></title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <?php print indent($head); ?>
        <!--[if lt IE 8]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
        <?php print indent($styles); ?>
        <link type="text/css" rel="stylesheet/less" href="<?php echo $theme_folder ?>/css/style.less" media="all" />
        <!--[if lt IE 8]><link type="text/css" rel="stylesheet" media="all" href="<?php echo $theme_folder ?>/css/ie7.css" /><![endif]-->
        
        <script src="<?php echo $theme_folder ?>/js/less-1.2.2.min.js"></script>
        <script src="<?php echo $theme_folder ?>/js/modernizr.custom.js"></script>

    </head>

        <body class="<?php print $classes; //can be modified in template.php mothership_preprocess_page or though the theme settings + http://drupal.org./node/171906  ?>" <?php print $attributes;?>>

        <?php print $page_top; //stuff from modules always render first ?>

        <?php print $page_header; // comes from template.php preprocess page?>
        <?php print $page; // uses the page.tpl  ?>
        <?php print $page_footer;  // comes from template.php preprocess page ?>

        <?php
            print indent($scripts);
        ?>

        <?php print $page_bottom; //stuff from modules always render last ?>

<?php if(theme_get_setting('load_scripts')) { ?>
	<script>
        Modernizr.load(['<?php echo $theme_folder; ?>/js/scripts.js']);
        </script>
<?php } ?>

        <?php
        //}
        /*
          Voorlopig zitten hier nog wat probleempjes in, te wijten aan oude drupalscripting. Hopelijk bij de 7 er uit!
          <!--[if (lt IE 9) & (!IEMobile)]>
          <script>
          Modernizr.load([
          '<?php echo $theme_folder ?>/js/jquery-extended-selectors.js',
          '<?php echo $theme_folder ?>/js/selectivizr-min.js',
          '<?php echo $theme_folder ?>/js/imgsizer.js'
          ]);
          </script>
          <![endif]-->

         */
        ?>

<?php if (theme_get_setting('force_google_frame')): ?>
        <!--[if lt IE 7 ]>
        <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
        <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
        <![endif]-->
<?php endif; ?>

        </body>
    </html>
<?php } ?>