<?php
function mytheme_preprocess_html(&$variables) {
        
}

function mytheme_preprocess_page(&$variables, $hook) {

}

function mytheme_process_html(&$variables) {
	
}

function mytheme_process_page(&$variables, $hook) {

}

function mytheme_html_head_alter(&$head_elements) {

}

/*
 * Custom function for rendering blocks through code
 */

function mytheme_block_render($module, $block_id) {
  $block = block_load($module, $block_id);
  $block_content = _block_render_blocks(array($block));
  $build = _block_get_renderable_array($block_content);
  $block_rendered = drupal_render($build);
  print $block_rendered;
}